const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");


module.exports = {
	context: path.resolve(__dirname, 'src'),
	
	entry: {
		global: ['./js/header.jsx', './scss/global.scss'],
		vendor: ['bootstrap/dist/css/bootstrap.min.css', 'react', 'react-dom', 'react-bootstrap' ]
	},

	output: {
		path: path.resolve(__dirname, 'build'),
		filename: '[name].js'
	},

	module: {
		rules: [{
			test: /\.(js|jsx)$/,
			exclude: [/node_modules/],
			use: [{
				loader: 'babel-loader',
				options: { presets: ['env', 'react'] }
			}]
		}, {
			test: /\.scss$/,
			use: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])			
		}, {
			test: /\.css$/,
			use: ExtractTextPlugin.extract({ fallback: 'style-loader', use: [ 'css-loader' ] })
		}, { 
			test: /\.(png|woff|woff2|eot|ttf|svg)$/, 
			loader: 'url-loader?limit=100000' 
		}]
	},

	plugins: [
		new webpack.optimize.CommonsChunkPlugin({
			name: 'vendor'
		}),
		new webpack.ProvidePlugin({
		}),
		new ExtractTextPlugin('[name].css')
	]
}
