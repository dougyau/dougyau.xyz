import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import Navigation from './navigation.jsx';

class Header extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			'account': [],
			'main': []
		};

		axios.get('/rest/header?_format=json')
			.then((response) => {
				this.setState(response.data);
			});
	}

	render() {
		return (
			<div className="clearfix">
				<div className="clearfix" style={{minHeight: "70px"}}>
					<Navigation items={this.state.account} id="header_account" align="align-right" fixedClass="fixed-right" />
				</div>
				<div className="head-fill"></div>
				<Navigation items={this.state.main} id="main_menu" align="align-left" fixedClass="fixed"/>
			</div>
			);
	}
}

var items = [{
	href: '#',
	text: 'link 1'
}, {
	href: '#',
	text: 'link 2'
}]

ReactDOM.render(<Header items={items}/>, document.getElementById('header'));