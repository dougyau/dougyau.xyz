import React from 'react';
import ReactDOM from 'react-dom';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

export default class Navigation extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			fix_top: false,
			id: props.id,
			align: props.align,
			fixedClass: props.fixedClass,
			height: -1
		};
	}

	componentDidMount() {
		window.addEventListener('scroll', this.handleScroll.bind(this));
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll.bind(this));
	}

	handleScroll(event) {
		let height = this.state.height == -1 ? 
			(document.getElementById(this.state.id).offsetTop - document.getElementById('main').offsetTop)
			: this.state.height;
		let fix_top = window.pageYOffset >= height;

		if (fix_top != this.state.fix_top || height != this.state.height)
			this.setState({
				fix_top: fix_top,
				height: height
			});
	}

	render() {
		const r = this.props.items.map((element, k) => 
			<NavItem eventKey={k} key={element.title.toString()} href={element.url}>{element.title}</NavItem>
			);

		const c = this.state.align + ' ' + (this.state.fix_top ? this.state.fixedClass : '');
		const style = { 
			top: document.getElementById('main').offsetTop
		};

		return (
			<Navbar bsStyle="inverse" fluid={true} className={c} id={this.state.id} style={this.state.fix_top ? style : {}}>
				<Nav>
					{ r }
				</Nav>
			</Navbar>
			);
	}
}
