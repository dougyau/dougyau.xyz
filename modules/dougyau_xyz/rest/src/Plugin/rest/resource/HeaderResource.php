<?php

namespace Drupal\dougyau_xyz\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @RestResource(
 *		id = "rest_header",
 *		label = @Translation("Rest Header"),
 *		uri_paths = {
 *			"canonical" = "/rest/header"
 *		})
 */
class HeaderResource extends ResourceBase {

	/**
	 * @return \Drupal\rest\ResourceResponse
	 */
	public function get() {
		$blocks = \Drupal::entityQuery('block')
			->condition('region', 'header')
			->condition('status', 1)
			->condition('theme', 'dougyau_xyz_theme')
			->execute();

		$return = [];
		$menu_tree = \Drupal::menuTree();

		foreach ($blocks as $block) {
			$machine_name = preg_replace('/dougyau_xyz_theme_(.+)_menu/', '${1}', $block);

			$parameters = $menu_tree->getCurrentRouteMenuTreeParameters($machine_name);
			$tree = $menu_tree->load($machine_name, $parameters);
			$manipulators = array(
			  	array('callable' => 'menu.default_tree_manipulators:checkAccess'),
			  	array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
			);

			$tree = $menu_tree->transform($tree, $manipulators);
			$items = $menu_tree->build($tree);

			$menu = [];
			foreach ($items['#items'] as $item) {
				$menu[] = [
					'title' => $item['title'] instanceof \Drupal\Core\StringTranslation\TranslatableMarkup
						? $item['title']->render()
						: $item['title'],
					'url' => $item['url']->toString(),
				];
			}

			$return[$machine_name] = $menu;
		}

		return new Response(json_encode($return), 200);
	}
}
